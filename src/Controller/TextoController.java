/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import Model.*;
/**
 * FXML Controller class
 *
 * @author sebas
 */
public class TextoController implements Initializable {
    private Cadena palabra;
    @FXML
    private Button BtnAccion;
    @FXML
    private TextArea txtEntrada;
    @FXML
    private TextField txtSalida;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        palabra = new Cadena();
    }    

    @FXML
    private void ClickBoton(ActionEvent event) {
        String texto = null;
        palabra.setTexto(txtEntrada.getText().toLowerCase());
        
        texto = palabra.palindromo();

        txtSalida.setText(texto);
    }
    
}
