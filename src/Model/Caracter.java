/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author sebas
 */
public class Caracter {
    private char caracteres;
    private int cantidad;

    public Caracter(char caracteres, int cantidad) {
        this.caracteres = caracteres;
        this.cantidad = cantidad;
    }
    
    public Caracter(){
        
    }

    public char getCaracteres() {
        return caracteres;
    }

    public void setCaracteres(char caracteres) {
        this.caracteres = caracteres;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

}
