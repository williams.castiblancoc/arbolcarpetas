/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import Controller.*;
import javafx.stage.FileChooser;
import javafx.stage.Window;

/**
 * FXML Controller class
 *
 * @author sebas
 */
public class PrincipalController implements Initializable {

    @FXML
    private Button btnCargar;
    @FXML
    private Button btnExportar;
    private String arbol = "";

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        
    }    

    @FXML
    private void clickCargar(ActionEvent event) {
        
        arbol = Logica.CargarCarpeta();
        
    }

    @FXML
    private void clickExportar(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Seleccione la ruta donde desea guardar el archivo");
        Window stage = null;
        fileChooser.showOpenDialog(stage);
        String ruta = "";
        ruta = fileChooser.getInitialFileName();
        System.out.println(ruta);
    }
    
}
